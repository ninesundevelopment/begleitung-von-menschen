export const AppVersion = {
  "version": {
    "major": 1,
    "minor": 0,
    "patch": 0
  },
  "status": {
    "stage": null,
    "number": 0
  },
  "build": {
    "date": "Thu Mar 07 2019 10:26:52 GMT+0100 (GMT+01:00)",
    "number": 41,
    "total": 61
  },
  "commit": null
};