import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './components/app/app.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {IndexComponent} from './pages/index/index.component';
import {ContactComponent} from './pages/contact/contact.component';
import {ImprintComponent} from './pages/imprint/imprint.component';
import {HeaderComponent} from './components/header/header.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {FooterComponent} from './components/footer/footer.component';
import {SidebarService} from './services/sidebar/sidebar.service';
import {ScrollService} from './services/scroll/scroll.service';
import {CreditsComponent} from './pages/credits/credits.component';
import {WindowScrollingService} from './services/window-scrolling/window-scrolling.service';
import {FragmentPolyfillModule} from './modules/fragment-polyfill-module/fragment-polyfill-module.module';
import {PageSectionComponent} from './components/page-section/page-section.component';
import {ArrowComponent} from './components/arrow/arrow.component';
import {UICarouselComponent} from './components/ui-carousel/ui-carousel.component';
import {UICarouselItemComponent} from './components/ui-carousel-item/ui-carousel-item.component';
import {DotsComponent} from './components/dots/dots.component';
import {SwiperDirective} from './directives/swiper.directive';
import {UILazyloadDirective} from './directives/ui-lazy-load.directive';
import {UrlService} from './services/url/url.service';
import {SafePipe} from './pipes/safe.pipe';
import {HttpClientModule} from '@angular/common/http';
import {OfflineComponent} from './pages/offline/offline.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {ScrollToTopComponent} from './components/scroll-to-top/scroll-to-top.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';


import {registerLocaleData} from '@angular/common';
import localeDe from '@angular/common/locales/de';
import {AboutComponent} from './pages/about/about.component';
import {ServicesComponent} from './pages/services/services.component';

registerLocaleData(localeDe);

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        NavigationComponent,
        IndexComponent,
        ContactComponent,
        ImprintComponent,
        AboutComponent,
        HeaderComponent,
        SidebarComponent,
        FooterComponent,
        CreditsComponent,
        PageSectionComponent,
        ArrowComponent,
        UICarouselComponent,
        UICarouselItemComponent,
        DotsComponent,
        SwiperDirective,
        UILazyloadDirective,
        SafePipe,
        OfflineComponent,
        NotFoundComponent,
        ScrollToTopComponent,
        ServicesComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        FragmentPolyfillModule.forRoot({
            smooth: true,
        }),
    ],
    providers: [
        SidebarService,
        ScrollService,
        WindowScrollingService,
        UrlService,
    ],
})
export class AppModule {
}

