import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UrlService } from '../../services/url/url.service';

@Component({
  selector: 'page-section',
  templateUrl: './page-section.component.html',
  styleUrls: ['./page-section.component.scss']
})
export class PageSectionComponent implements OnInit {

  @HostBinding('class.parallax') hostParallaxBinding: boolean = false;

  @Input() public parallax: boolean = false;
  @Input() public parallaxImage: string = '';

  @Input() public bttButton: boolean = true;

  @Input() public columns: number = 1;

  public currentUri: string = '';

  constructor(
    public route: ActivatedRoute
  ) {
    this.currentUri = UrlService.getRouteFromRoot(route.pathFromRoot);
  }

  ngOnInit() {
    this.hostParallaxBinding = this.parallax;
  }

}
