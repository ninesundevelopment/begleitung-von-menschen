import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar/sidebar.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public sidebarVisible = false;

  constructor(
    private sidebarSrv: SidebarService
  ) { }

  ngOnInit() {
  }

  public toggleSidebar() {
    this.sidebarSrv.visible.next(!this.sidebarSrv.visible.value)
  }
}
