import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {

  @Input() public location: 'HEADER' | 'SIDEBAR' | 'FOOTER';
  public buttonClass: string;

  constructor( ) { }

  ngOnInit() {
    switch (this.location) {
      case 'HEADER':
        this.buttonClass = 'transparent';
        break;
      case 'SIDEBAR':
        this.buttonClass = 'dark';
        break;
      case 'FOOTER':
        this.buttonClass = 'dark';
        break;

      default:
        this.buttonClass = '';
    }
  }

}
