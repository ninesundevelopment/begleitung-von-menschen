import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar/sidebar.service';
import { ScrollService } from '../../services/scroll/scroll.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  public blockerIsVisible     = true;
  public sidebarVisible       = false;

  constructor(
    private sidebarSrv: SidebarService,
    private scrollSrv: ScrollService,
  ) {
    this.sidebarSrv.visible.subscribe((value) => {
      if (!value) window.setTimeout(() => {
        this.sidebarVisible = false
      }, 300);
      else this.sidebarVisible = true;
    });

    this.scrollSrv.scrollTop.subscribe((value) => {
      document.querySelector('body').scrollTop = value;
    });
  }

  ngOnInit() {
    window.setTimeout(() => {
      this.blockerIsVisible = false;
    }, 500);
  }
}
