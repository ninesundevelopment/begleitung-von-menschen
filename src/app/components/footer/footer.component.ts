import { Component, OnInit } from '@angular/core';
import { ScrollService } from '../../services/scroll/scroll.service';
import {AppVersionService, IAppVersion} from "../../services/app-version/app-version.service";
import {AppPackageService, IAppPackage} from "../../services/app-package/app-package.service";

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {

  public version: IAppVersion = null;
  public packageJson: IAppPackage = null;

  constructor(
      private versionService: AppVersionService,
      private packageService: AppPackageService,
      private scrollSrv: ScrollService,
  ) {
    this.version = this.versionService.version;
    this.packageJson = this.packageService.package;
  }

  public scrollTop() {
    this.scrollSrv.scrollTop.next(0);
  }
}
