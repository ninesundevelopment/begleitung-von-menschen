import { TestBed, inject } from '@angular/core/testing';

import { AppPackageService } from './app-package.service';

describe('AppPackageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppPackageService]
    });
  });

  it('should be created', inject([AppPackageService], (service: AppPackageService) => {
    expect(service).toBeTruthy();
  }));
});
