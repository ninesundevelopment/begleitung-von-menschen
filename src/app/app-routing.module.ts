import {NgModule} from '@angular/core';

import {RouterModule} from '@angular/router';
import {IndexComponent} from './pages/index/index.component';
import {ImprintComponent} from './pages/imprint/imprint.component';
import {ContactComponent} from './pages/contact/contact.component';
import {CreditsComponent} from './pages/credits/credits.component';
import {OfflineComponent} from './pages/offline/offline.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {AboutComponent} from './pages/about/about.component';
import {ServicesComponent} from './pages/services/services.component';

const appRoutes = [{
    path: '',
    pathMatch: 'full',
    redirectTo: '/home',
}, {
    path: 'home',
    component: IndexComponent,
}, {
    path: 'services',
    component: ServicesComponent,
}, {
    path: 'about',
    component: AboutComponent,
}, {
    path: 'contact',
    component: ContactComponent,
}, {
    path: 'imprint',
    component: ImprintComponent,
}, {
    path: 'credits',
    component: CreditsComponent,
}, {
    path: '404',
    component: NotFoundComponent,
}, {
    path: 'offline',
    component: OfflineComponent,
}, {
    path: '**',
    redirectTo: '/404',
},
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, {
            useHash: true,
            enableTracing: false,
            scrollPositionRestoration: 'enabled',
            anchorScrolling: 'enabled',
            urlUpdateStrategy: 'eager',
        }),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppRoutingModule {
}
