/*
 * Dis gulpfile just watches all ts, scss and html files and
 * updates the build number on each change.
 */

'use strict';

const gulp   = require('gulp');
const apv    = require('appversion');
const exec   = require('child_process').exec;
let inAction = false;

gulp.task('default', () => {
  gulp.watch('./src/app/**/*', () => {
    if (!inAction) {
      inAction = true;
      setTimeout(() => {
        exec('npm run prebuild', () => {
          apv.composePattern('M.m.p-Ss n:t', (ptt) => {
            console.log(ptt);
          });
          return 0;
        });
        inAction = false;
      }, 2500);
    }
  });
});
